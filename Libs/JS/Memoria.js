"use strict";

class HeadWriter {
  constructor() {
    this.title = "Memoria";
    this.tagsHead = document.head.children;
  }
  
  titleInsert() {
    document.title = "Now Loading...";
    window.addEventListener(
      "load",
      () => {
        document.title = "Now Loading...";
      }
    )
  }
}

class BodyWriter {
  constructor() {
    this.tagsBody = document.body.children;
  }
}

(function() {
  const HW = new HeadWriter()
  const BW = new BodyWriter()
  HW.init()
  BW.init()
})();
