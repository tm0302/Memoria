#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ConfigSuppoter import ConfigGetter
from Localizer import LocaleGetter, Localizer

class WebServer:
  def __init__(self):
    Host = ConfigGetter("WebServer.Host")
    Port = ConfigGetter("WebServer.Port")
    Locale = LocaleGetter()
        
  def __call__(self):
    print(Localizer())
