#!/usr/bin/env python
# -*- coding: utf-8 *-

from Protector import Compresser, Decompresser, Cryptor, Decryptor
from ConfigSupporter import ConfigAdjustmenter
from Updater import AppUpdater

class Initializer:
  def __init__(self):
  
  def __call__(self):
    AppUpdater()
    Decompresser()
    Decryptor()
    ConfigAdjustmenter()

class Finalizer:
  def __init__(self):
  
  def __call__(self):
    ConfifAdjustmenter()
    Cryptor()
    Compresser()
