#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib.request
from Localizer import Localizer
from Errors import HTTPError

class AppUpdater:
  def __init__(self):
    self.name = "Memoria"
    self.author = "tm0302"
    self.branch = "master"
    self.versions = "Version-List.yaml"
    self.source = "{0}.zip".format(self.branch)
    self.urlVersions = "https://raw.githubusercontent.com/{0}/{1}/{2}/{3}".format(self.author, self.name, self.branch, self.versions)
    self.urlSource = "https://github.com/{0}/{1}/archive/{2}.zip".format(self.author, self.name, self.branch)
    self.secTimeout = 30

  def __call__(self):
    self.getVersions()
    self.getSource()
    
  def getVersions(self):
    try:
      urllib.request.urlretrieve(self.urlVersions, self.versions)
    except Error.HTTPError(e):
      print(e)
      print(Localizer())
      
  def getSource(self):
    try:
      urllib.request.urlretrieve(self.urlSource, self.source)
    except Error.HTTPError(e):
      print(e)
      print(Localizer())
