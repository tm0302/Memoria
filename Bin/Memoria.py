#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
------------------------------------------------------------
* Name: Memoria
* Version: v1.0.0
* Author: Hideki
* License: Mozilla Public License 2.0
------------------------------------------------------------
"""

from Adjustmenter import Initializer, Finalizer
from Server import WebServer

if __name__ = "__main__":
  Initializer()
  WebServer()
  Finalizer()
