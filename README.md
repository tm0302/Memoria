# Memoria
Memoria is an open source software to note your memories.  
Let's open a new road from your memories and your life.  

## Features
- Absolutely make personal information safe.
  - Like a certain SNS, this will not misuse personal information or let it run out.  
  - Because we all manage it locally.
  - And I think privacy protection is important.
- Support PWA.
  - Accelerate page loading.
  - Can also be used offline.
  - You can receive a push notification.
  - [PWA Details](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/)
- Modern UI Design.
  - This conforms to material design.
  - User friendly.
  - Switch theme possible.
  - You can switch themes.
  - You can also develop themes.
  - [Material Design Details](https://material.io/)
- Open source.
  - Open source has many dreams.
  - But closed source doesn't have many dreams.
  - I think open source is a good choice.
  
# Don't implement the function.
- Don't support addons.
 - It does not correspond to privacy protection. 
  
## Version
Please refer to "Versions.yaml"
